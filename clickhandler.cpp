#include "clickhandler.h"
#include <QMessageBox>
#include <QMouseEvent>
#include <QDebug>

ClickHandler::ClickHandler(QObject *parent):
	QObject(parent),
	quickWindow(qobject_cast<QQuickWindow *>(parent)),
	dragging(false)
{
	if (quickWindow) {
		connect(quickWindow,  SIGNAL(mainMenuOptionSelected(int)),
							  SLOT(onMainMenuOptionSelected(int)));
//		quickWindow->setFlags(quickWindow->flags()&~Qt::WindowTitleHint);
		quickWindow->setFlags(quickWindow->flags()&~(Qt::WindowMinimizeButtonHint|
													 Qt::WindowMaximizeButtonHint));
		quickWindow->installEventFilter(this);
	}
}

bool ClickHandler::eventFilter(QObject *object, QEvent *ev)
{
	Q_UNUSED(object)

	QRect dragArea = quickWindow->property("dragArea").toRect();

	switch (ev->type()) {
		case QEvent::MouseButtonPress:
		{
			QMouseEvent *mouseEvent = static_cast<QMouseEvent *>(ev);
			if (dragArea.contains(mouseEvent->pos()))
			{
				dragging = true;
				dragPoint = mouseEvent->screenPos().toPoint();
				anchorPoint = quickWindow->position();
			}
			break;
		}

		case QEvent::MouseButtonRelease:
			if (dragging)
				dragging = false;
			break;

		case QEvent::MouseMove:
			if (dragging)
				quickWindow->setPosition(anchorPoint+static_cast<QMouseEvent *>(ev)->screenPos().toPoint()-dragPoint);
			break;

		default:
			break;
	}

	return false;
}

void ClickHandler::onMenuItemClicked(int id, QObject *source)
{
	qDebug() << "ClickHandler::onMenuItemClicked(" << id << "," << source << ")";
}

void ClickHandler::onMainMenuOptionSelected(int itemId)
{
	qDebug() << "ClickHandler::onMainMenuOptionSelected(" << itemId <<  ")";
	switch (itemId) {
		case 6:
			QMessageBox::aboutQt(NULL);
			break;
		default:
			break;
	}
}
