#ifndef CLICKHANDLER_H
#define CLICKHANDLER_H

#include <QObject>
#include <QQuickWindow>

class ClickHandler : public QObject
{
    Q_OBJECT
public:
	explicit ClickHandler(QObject *parent = nullptr);

	// QObject interface
	bool eventFilter(QObject *object, QEvent *ev);

signals:

public slots:
	void onMenuItemClicked(int id, QObject*source);
	void onMainMenuOptionSelected(int itemId);

private:
	QQuickWindow *quickWindow;
	bool dragging;
	QPoint dragPoint, anchorPoint;
};

#endif // CLICKHANDLER_H
