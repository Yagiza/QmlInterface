#include <QList>
#include <QVariant>
#include "rosterlabels.h"

RosterLabel::RosterLabel(QObject *parent) : QObject(parent)
{

}

QString RosterLabel::icon() const
{
	return QString();
}

void RosterLabel::setIcon(const QString &icon)
{
	Q_UNUSED(icon)
}

QString RosterLabel::text() const
{
	return QString();
}

void RosterLabel::setText(const QString &text)
{
	Q_UNUSED(text)
}

QFont RosterLabel::font() const
{
	return QFont();
}

void RosterLabel::setFont(const QFont &font)
{
	Q_UNUSED(font)
}

QColor RosterLabel::foreground() const
{
	return QColor();
}

void RosterLabel::setForeground(const QColor &color)
{
	Q_UNUSED(color)
}

QColor RosterLabel::background() const
{
	return QColor();
}

void RosterLabel::setBackground(const QColor &color)
{
	Q_UNUSED(color)
}

RosterLabels::RosterLabels(QObject *parent) : QObject(parent)
{	
}

QList<RosterLabel *> RosterLabels::topRight()
{
	return QList<RosterLabel *>();
}
