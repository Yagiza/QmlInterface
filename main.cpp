#include <QDebug>
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QtQml>
#include "clickhandler.h"
#include "rostersmodel.h"
#include "simplemenumodel.h"

int main(int argc, char *argv[])
{
#if defined(Q_OS_WIN) && QT_VERSION >= 0x050600
	QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

	QApplication app(argc, argv); // Use QGuiApplication if no need in Widgets
								  // Right now widgets ar used for "About Qt"
								  // dialog only

	qmlRegisterType<RosterIconData>("ru.rwsoftware.eyecu", 1, 0, "RosterIconData");
	qmlRegisterType<SimpleMenuItem>("ru.rwsoftware.eyecu", 1, 0, "SimpleMenuItem");
	qmlRegisterType<SimpleMenuGroup>("ru.rwsoftware.eyecu", 1, 0, "SimpleMenuGroup");
	qmlRegisterType<SimpleMenuModel>("ru.rwsoftware.eyecu", 1, 0, "SimpleMenuModel");

	QQmlApplicationEngine engine;
    QQmlContext *ctxt = engine.rootContext();

	RostersModel model;
	ctxt->setContextProperty("rostersModel", &model);

	SimpleMenuModel mainMenu;
	mainMenu.addItem(0, 0, "Join conference...", "mucconference");
	mainMenu.addItem(0, 1, "File transfers", "filestreamsmanager");
	mainMenu.addItem(1, 2, "XML console", "console");
	mainMenu.addItem(1, 3, "Settings", "optionsdialog");
	mainMenu.addItem(1, 4, "Setup plugins", "pluginmanagersetup");
	mainMenu.addItem(1, 5, "Switch profile", "optionsprofiles");
	mainMenu.addItem(2, 6, "About Qt", "pluginmanageraboutqt");
	mainMenu.addItem(2, 7, "About", "pluginmanagerabout");
	mainMenu.addItem(3, 8, "Quit", "mainwindowquit");

	ctxt->setContextProperty("mainMenuModel", &mainMenu);

    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));
	if (engine.rootObjects().isEmpty())
		return -1;

	ClickHandler *clickHandler = new ClickHandler(engine.rootObjects().first());

	SimpleMenuModel rosterItemMenu;
	clickHandler->connect(&rosterItemMenu, SIGNAL(itemSelected(int,QObject*)), SLOT(onMenuItemClicked(int,QObject*)));

	rosterItemMenu.addItem(0, 0, "Send message", "normalmessage");
	rosterItemMenu.addItem(0, 1, "Discovery", "discoinfo");
	rosterItemMenu.addItem(0, 2, "Delete", "removecontact");
	rosterItemMenu.addItem(1, 3, "Application version", "clientinfo");

    RosterIndex *index = new RosterIndex();
	index->setData(RosterIndex::NameRole, "Dimchik");
	index->setData(RosterIndex::AvatarRole, QStringList({"0f675a84946d9ac65622e2fad3131c6a6adff3cb", "0df7788e18beb12e859747cec23226ad4cb9461d"}));
	index->setData(RosterIndex::ShowRole, "away");
	index->setData(RosterIndex::StatusRole, "I'm away!");
	index->setData(RosterIndex::JidsRole, QStringList({"vasya@xmpp.org", "vasya@jabber.com"}));
    index->setData(RosterIndex::ResourceRole, "eyeCU");
	index->setData(RosterIndex::AnnotationsRole, QVariant::fromValue(new RosterIconData("annotations",
											"<html>"
											"<p>"
											"You dress me up<br>"
											"I'm your puppet<br>"
											"You buy me things<br>"
											"I love it<br>"
											"You bring me food<br>"
											"I need it<br>"
											"You give me love<br>"
											"I feed it"
											"</p>"
											"</html>")));
	index->setData(RosterIndex::MoodRole, QVariant::fromValue(new RosterIconData("angry", "Angry (Really angry)")));
	index->setData(RosterIndex::ActivityRole, QVariant::fromValue(new RosterIconData("grooming/taking_a_bath", "Grooming: Taking a bath (Ahhhh...)")));
	index->setData(RosterIndex::ClientRole, QVariant::fromValue(new RosterIconData("eyecu", "eyeCU")));
	index->setData(RosterIndex::MenuRole, QVariant::fromValue(&rosterItemMenu));

    model.addRosterItem(index);
    index = new RosterIndex();
	index->setData(RosterIndex::NameRole, "Pyetya");
	index->setData(RosterIndex::AvatarRole, QStringList({"0dba82fcef36ac87f2d1e07f2ed89bca005e9f1a"}));
	index->setData(RosterIndex::ShowRole, "online");
	index->setData(RosterIndex::StatusiconsetRole, "icq");
	index->setData(RosterIndex::StatusRole, "I'm online!");
    index->setData(RosterIndex::ResourceRole, "eyeCU");
	index->setData(RosterIndex::JidsRole, QStringList("pyetya@xmpp.org"));
	index->setData(RosterIndex::AnnotationsRole, QVariant::fromValue(new RosterIconData("annotations", "* Chop fruit and place in a bowl.")));
	index->setData(RosterIndex::MoodRole, QVariant::fromValue(new RosterIconData("sick", "Sick (Caught cold)")));
	index->setData(RosterIndex::ActivityRole, QVariant::fromValue(new RosterIconData("inactive/hanging_out", "To the downtown")));
	index->setData(RosterIndex::ClientRole, QVariant::fromValue(new RosterIconData("vacuum", "Vacuum-IM")));
	index->setData(RosterIndex::MenuRole, QVariant::fromValue(&rosterItemMenu));
    model.addRosterItem(index);

	return app.exec();
}
