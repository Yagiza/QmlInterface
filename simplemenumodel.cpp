#include "simplemenumodel.h"

SimpleMenuModel::SimpleMenuModel(QObject *parent) : QObject(parent)
{

}

QList<QVariant> SimpleMenuModel::groups() const
{
	QList<QVariant> list;
	for (QMap<int, SimpleMenuGroup *>::ConstIterator it=_groups.constBegin(); it!=_groups.constEnd(); ++it)
		list.append(QVariant::fromValue(*it));
	return list;
}

void SimpleMenuModel::addItem(int group, SimpleMenuItem *item)
{
	if (!_groups.contains(group)) {
		_groups.insert(group, new SimpleMenuGroup(this));
		emit groupsChanged();
	}
	_groups[group]->addItem(item);
}

void SimpleMenuModel::addItem(int group, int id, QString text, QString icon)
{
	addItem(group, new SimpleMenuItem(id, text, icon));
}

SimpleMenuItem *SimpleMenuModel::item(int id)
{
	for (QMap<int,SimpleMenuGroup*>::Iterator it=_groups.begin(); it!=_groups.end(); ++it)
	{
		SimpleMenuItem *item = (*it)->item(id);
		if (item)
			return item;
	}
	return NULL;
}

//
// SimpleMenuItem
//
SimpleMenuItem::SimpleMenuItem(int id, const QString &text, const QString &icon, QObject *parent):
	QObject(parent), _id(id), _text(text), _icon(icon)
{}

SimpleMenuItem::SimpleMenuItem(const SimpleMenuItem &other):
	QObject(other.parent()), _id(other._id), _text(other._text), _icon(other._icon)
{}

int SimpleMenuItem::id() const
{
	return _id;
}

const QString &SimpleMenuItem::icon() const
{
	return _icon;
}

const QString &SimpleMenuItem::text() const
{
	return _text;
}

void SimpleMenuItem::setId(int id)
{
	if (_id!=id)
		emit idChanged(_id = id);
}

void SimpleMenuItem::setIcon(const QString &icon)
{
	if (_icon!=icon)
		emit iconChanged(_icon=icon);
}

void SimpleMenuItem::setText(const QString &text)
{
	if (_text!=text)
		emit textChanged(_text=text);
}

//
// SimpleMenuGroup
//
SimpleMenuGroup::SimpleMenuGroup(SimpleMenuModel *parent):
	QObject(parent)
{

}

const QList<QVariant> &SimpleMenuGroup::items() const
{
	return _items;
}

void SimpleMenuGroup::setItems(const QList<QVariant> &items)
{
	if (_items != items)
		emit itemsChanged();
}

void SimpleMenuGroup::addItem(SimpleMenuItem *item)
{
	_items.append(QVariant::fromValue(item));
	emit itemsChanged();
}

SimpleMenuItem *SimpleMenuGroup::addItem(int id, QString text, QString icon)
{
	SimpleMenuItem *item = new SimpleMenuItem(id, text, icon, this);
	addItem(item);
	return item;
}

bool SimpleMenuGroup::removeItem(int index)
{
	if (_items.size()>index) {
		_items.removeAt(index);
		emit itemsChanged();
		return true;
	}
	return false;
}

SimpleMenuItem *SimpleMenuGroup::item(int id)
{
	for (QList<QVariant>::Iterator it=_items.begin(); it!=_items.end(); ++it)
	{
		SimpleMenuItem *item = (*it).value<SimpleMenuItem *>();
		if (item->id()==id)
			return item;
	}
	return NULL;
}
