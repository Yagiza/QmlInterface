import QtQuick 2.0
import QtQuick.Layouts 1.2
import ru.rwsoftware.eyecu 1.0

Item {
	property string storage
	property string name
	property string icon
	property RosterIconData iconData
	property bool multiline: false

	id: root
	width: parent.width
	height: textLayout.height

	GridLayout {
		id: textLayout
		rows: (multiline && !icon)?2:1
		columns: (rows==1)?2:1
		width: parent.width

		Item {
			id: label
			width: labelIcon.visible?labelIcon.width:labelText.width
			height: labelIcon.visible?labelIcon.width:labelText.height
			Layout.alignment: Qt.AlignTop

			Image {
				id: labelIcon
				source: storage + "/" + icon
				visible: storage && icon				
			}

			SmallText {
				id: labelText
				text: name?(name + (multiline?"":": ")):""
				font.pointSize: 12; font.bold: true
				visible: !icon && name
			}
		}

		SmallText {
			id: dataText
			Layout.fillWidth: true
			text: iconData.text
			font.pointSize: 12;
			wrapMode: multiline?Text.Wrap:Text.NoWrap
			visible: iconData
		}
	}
}
