import QtQuick 2.0

Rectangle {
    id: root
    color: palette.highlight

	property font font: Qt.font({pointSize: 14})
	property string title

	signal mainMenuClicked

	SystemPalette { id: palette; colorGroup: SystemPalette.Active }

    Row {
        anchors.fill: parent

        Item {
            height: parent.height
            width: height

            HeaderButton {
                id: mainMenu
                anchors.fill: parent
				onClicked: mainMenuClicked()
            }
        }

        Item {
            height: parent.height
            width: height
            Text {
                anchors.centerIn: parent
				text: root.title
                font.bold: true;
				verticalAlignment: Text.AlignVCenter
				color: palette.highlightedText
            }
        }
    }
}
