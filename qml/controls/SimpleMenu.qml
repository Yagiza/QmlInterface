import QtQuick 2.0
import QtQuick.Layouts 1.2

Item {
	id: root
	property var model
	property int margins: 0
	property int verticalMargins: full?margins:0
	property int horizontalMargins: margins
	property int topMargin: verticalMargins
	property int bottomMargin: verticalMargins
	property int leftMargin: horizontalMargins
	property int rightMargin: horizontalMargins
	property int separatorHeight: 10
	property bool full

	signal clicked(int id)

	width: parent.width
	height: full?menuGrid.height+topMargin+bottomMargin:parent.height

	ColumnLayout {
		id: menuGrid
		x: leftMargin
		y: topMargin
		width: parent.width-leftMargin-rightMargin

		spacing: 1

		Repeater {
			model: root.model.groups

			Grid {
				visible: !index || full
				rows: full?-1:1
				columns: rows==1?-1:1
				spacing: 1
				layoutDirection: full?Qt.LeftToRight:Qt.RightToLeft
				width: root.width
				Layout.fillWidth: true

				Item {
					id: separator
					width: parent.width
					height: separatorHeight
					visible: index

					Rectangle {
						y: parent.height/2
						height: 1
						width: parent.width
						color: "black"
					}
				}

				Repeater {
					model: modelData.items

					MenuItem {
						id: menuItem
						full: root.full
						menuitemData: modelData
						onClick: root.clicked(id)
						shortHeight: root.height
					}
				}
			}
		}
		Item {
			Layout.fillHeight: root.state=="Details"
		}
	}
}
