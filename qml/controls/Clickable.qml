import QtQuick 2.5

Item {
	id: root
	signal clicked()
	signal pressedAndHeld()
	signal pressed()
	property int spotRadiusMin: 20
	property int spotRadiusMax: 200
	property bool selectable: false

	clip: true

	SystemPalette { id: palette; colorGroup: SystemPalette.Active }

	Rectangle {
		id: highlight
		color: palette.shadow
		opacity: 0
		anchors.fill: parent
		state: parent.state

		states: [
			State {
				name: "highlighted"
				PropertyChanges {
					target: highlight
					opacity: 0.5
				}
			},

			State {
				name: "selected"
				PropertyChanges {
					target: highlight
					opacity: 0.3
				}
			}
		]

		transitions: [
			Transition {
				NumberAnimation { duration: 300; properties: "opacity" }
			}
		]
	}

	Rectangle {
		property bool selected: false
		property int tx
		property int ty

		id: spot
		width: radius*2
		height: radius*2
		opacity: 0.5
		radius: spotRadiusMin
		visible: false
		color: palette.shadow
		x: tx - radius
		y: ty - radius

		states: State {
			name: "animated"
			PropertyChanges {
				target: spot
				radius: spotRadiusMax
				opacity: 0
				visible: true
			}
			onCompleted: if (spot.selected) {
							 spot.selected=false
							 root.state="selected"
							 root.pressedAndHeld()
						 } else root.clicked()
		}

		transitions: [
			Transition { from: "animated"; to: ""; enabled: false },
			Transition { from: ""; to: "animated"
				NumberAnimation { duration: 300; properties: "opacity,radius" }
			}
		]

		function animate(x, y, s) {
			selected=s
			tx=x
			ty=y
			state=""
			state="animated"
		}
	}

	MouseArea {
		anchors.fill: parent
		onClicked: spot.animate(mouseX, mouseY, false)
		onPressAndHold: if (selectable) spot.animate(mouseX, mouseY, true)
		onPressed: { root.state="highlighted"; root.pressed() }
		onReleased: if (spot.selected) root.state="selected"
					else root.state=""
	}
}
