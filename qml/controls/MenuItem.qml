import QtQuick 2.0

Item {
	property int margins: 10
	property bool full: false
	property var menuitemData
	property int shortHeight
	property font font: Qt.font({pointSize: 16})
	signal click(int id)

	id: root

	width: full?parent.width:menuItem.width+margins+margins
	height: full?menuItem.height+margins+margins:shortHeight

	Item {
		id: menuItem
		width: itemRow.width
		height: itemRow.height
		anchors.verticalCenter: parent.verticalCenter
		x: margins

		Row {
			id: itemRow
			spacing: 4

			Image {
				width: 32
				height: 32
				source: "qrc:/menuicons/"+menuitemData.icon
			}

			Text {
				text: menuitemData.text
				font.pointSize: 16
				visible: full
			}
		}
	}

	Clickable {
		id: clicker
		anchors.fill: parent
		onClicked: root.click(menuitemData.id)
	}
}
