import QtQuick 2.0
import QtQuick.Layouts 1.2
import ru.rwsoftware.eyecu 1.0

Item {
	property double imageSize
	property double imageHeight: imageSize
	property double imageWidth: imageSize
	property string storage
	property RosterIconData iconData

	id: root
	width: icon.width
	height: icon.height

	Image {
		id: icon
		source: storage + "/" + iconData.icon
		width: root.imageWidth?imageWidth:sourceSize.width
		height: root.imageHeight?imageHeight:sourceSize.height
	}
}
