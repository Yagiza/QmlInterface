import QtQuick 2.0
import QtQuick.Layouts 1.2

Item {
    SystemPalette { id: activePalette; colorGroup: SystemPalette.Active }
    property real internalSize: height * 0.4
    property color imageColor: activePalette.button
    signal clicked
    id: root

    Item {
        id: image
        height: internalSize * 0.5
        width: internalSize
        anchors.centerIn: parent

        ColumnLayout {
            id: layout
            anchors.fill: parent

            Repeater {
                model: 3

                Rectangle {
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    Layout.preferredWidth: image.width
                    Layout.preferredHeight: image.height * 0.18
                    color: imageColor
                }
            }
        }
    }

    states: State {
        name: "Active"
        PropertyChanges { target: root; imageColor: activePalette.light } // Make picture bigger
    }

    transitions: Transition {
        // Make the state changes smooth
        ColorAnimation { property: "imageColor"; duration: 200 }
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true
        onEntered: root.state = "Active";
        onExited: root.state = "";
        onClicked: root.clicked()
		preventStealing: true
    }
}
