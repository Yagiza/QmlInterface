import QtQuick 2.0
import QtQuick.Window 2.1
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.1
import QtQml.Models 2.2
import "controls"
import "rostersview"

Window {
	id: mainWindow
	visible: true
    width: 360
	height: 640
	title: qsTr("eyeCU")

	property rect dragArea: Qt.rect(header.x, header.y, header.width, header.height)

	signal mainMenuOptionSelected(int itemId)

	SystemPalette { id: palette; colorGroup: SystemPalette.Active }

	MessageDialog {
		id: quitAppDialog
		title: "Quit eyeCU"
		text: "Are you sure?"
		standardButtons: StandardButton.Ok | StandardButton.Cancel
		icon: StandardIcon.Question
		onAccepted: Qt.quit()
	}

	MessageDialog {
		id: obsoleteDialog
		text: "This option is obsolete and will be removed!"
		icon: StandardIcon.Warning

		function show(message) {
			title = message
			visible = true
		}
	}

	ColumnLayout {
		anchors.fill: parent
		spacing: 0

		WindowHeader {
			id: header
			Layout.fillWidth: true
			Layout.preferredHeight: 64
			title: mainWindow.title

			onMainMenuClicked: mainMenuDrawer.show()
		}

		RostersView {}
	}

	Drawer {
		id: mainMenuDrawer
		ColumnLayout {
			id: layout
			anchors.fill: parent
			Rectangle {
				Layout.fillHeight: true
				Layout.fillWidth: true
				gradient: Gradient {
					GradientStop {position: 0; color: "lightgray"}
					GradientStop {position: 1; color: "white"}
				}

				ColumnLayout {
					anchors.fill: parent
					Image {
						Layout.fillHeight: true
						Layout.fillWidth: true
						source: "qrc:/menuicons/eyecu"
						sourceSize: Qt.size(width, height)
						fillMode: Image.PreserveAspectFit
					}
					Text {
						Layout.alignment: Qt.AlignHCenter
						text: "eyeCU v1.4.0.122"
						font.family: "Arial Black"
						font.pointSize: 10
					}
					Text {
						Layout.alignment: Qt.AlignHCenter
						text: "<html>&copy; Road Works Software, 2018</html>"
						font.family: "Arial"
					}
				}
			}

			Item {
				Layout.fillWidth: true
				Layout.preferredHeight: mainMenu.height

				SimpleMenu {
					id: mainMenu
					model: mainMenuModel
					full: true
					onClicked: {
						mainMenuDrawer.hide()
						if (id==5) obsoleteDialog.show("About eyeCU")
						else if (id==7) obsoleteDialog.show("About eyeCU")
						else if (id==8) quitAppDialog.visible = true;
						else mainWindow.mainMenuOptionSelected(id)
					}
				}
			}
		}
	}

	onClosing: { close.accepted= false; mainWindow.showMinimized() }
}
