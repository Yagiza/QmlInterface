import QtQuick 2.5
import QtQuick.Window 2.0
import QtQuick.Layouts 1.2
import QtQuick.Controls 1.4
import QtQml.Models 2.2
import "../controls"
import ru.rwsoftware.eyecu 1.0

Rectangle {
	id: roster
	color: palette.light
	Layout.fillWidth: true
	Layout.fillHeight: true
	clip: true

	SystemPalette { id: palette; colorGroup: SystemPalette.Active }

	// Delegate for the recipes.  This delegate has two modes:
	// 1. List mode (default), which just shows the picture and title of the recipe.
	// 2. Details mode, which also shows the ingredients and method.
	Component {
        id: rosterItemDelegate

		Item {
			id: rosterItem

			// Create a property to contain the visibility of the details.
			// We can bind multiple element's opacity to this one property,
			// rather than having a "PropertyChanges" line for each element we
			// want to fade.
			property real detailsOpacity : 0
			property int  avatarFullHeight : 128

			width: rostersView.width
			height: 70

            Item {
                id: topLayout
                x: 10; y: 10; height: 50; width: parent.width

                Flickable {
                    id: avatarFlick
                    height: parent.height
                    width: 50
                    flickableDirection: Flickable.VerticalFlick
                    contentWidth: avatars.width
                    clip: true

                    Repeater {
                        id: avatars
                        model: avatar

                        Image {
                            id: avatarImage
                            source: "qrc:/avatars/"+modelData
                            height: avatarFlick.height+20
                            width: height
                            fillMode: Image.PreserveAspectFit
                        }
                    }
                }

                Text {
                    id: itemName
                    text: name
                    font.bold: true; font.pointSize: 16
                    x: avatarFlick.x+50 + 10
                    y: 0
                }

                state: rosterItem.state

                states: State {
                    name: "Details"
                    PropertyChanges { target: itemName; x: 0; y: rosterItem.avatarFullHeight }					
                }

                transitions: Transition {
                    // Make the state changes smooth
                    ParallelAnimation {
                        NumberAnimation { duration: 300; properties: "x,y" }
                    }
                }

//						spacing: 10

//						Item {
//							id: recipeImage
//							width: 50; height: 50
//							property int fullHeightIndex
//							property int fullHeight: Math.min(avatars.maxSourceHeight(), 128)

//							Repeater {
//								id: avatars
//								model: avatar
//								function maxSourceHeight() {
//									var h=0
//									for (var i=0; i<count; ++i)
//										if (h<itemAt(i).sourceSize.height) {
//											h=itemAt(i).sourceSize.height
//											recipeImage.fullHeightIndex=i
//										}
//									return h
//								}

//								Image {
//									id: avatarImage
//									width: recipeImage.width
//									height: recipeImage.height
//									fillMode: Image.PreserveAspectFit
//									source: "qrc:/avatars/"+modelData
//									visible: index?state=="Details":true
//									opacity: index?0:1

//									property int maxWidth: 128
//									property int maxHeight: 128
//									property int fullWidth: Math.min(maxWidth, sourceSize.width)
//									property int fullHeight: Math.min(maxHeight, sourceSize.height)

//									state: rosterItem.state

//									states: [
//										State {
//											name: "Details"
//											PropertyChanges {
//												target: avatarImage
//												x: index?avatars.itemAt(index-1).x+avatars.itemAt(index-1).width:0
//												width: fullWidth
//												height: fullHeight
//												opacity: 1
//											}
//										}
//									]

//									transitions: Transition {
//										// Make the state changes smooth
//										ParallelAnimation {
//											NumberAnimation { duration: 300; properties: "x,width" }
//										}
//									}
//								}
//							}
//						}

//						Column {
//							width: parent.width - recipeImage.width - 20; height: recipeImage.height
//							spacing: 5

//							RowLayout {
//								width: parent.width
//								Layout.fillWidth: true
//								Text {
//									text: name
//									font.bold: true; font.pointSize: 16
//								}

//								Row {
//									id: rosterIcons
//									Layout.fillWidth: true
//									Layout.fillHeight: true
//									spacing: 4
//									layoutDirection: Qt.RightToLeft
//									opacity: 1-rosterItem.detailsOpacity

//									RosterIcon {
//										id: clientIcon
//										storage: "qrc:/clienticons"
//										iconData: client
//										imageSize: 32
//									}

//									RosterIcon {
//										id: moodIcon
//										storage: "qrc:/moodicons"
//										iconData: mood
//										imageSize: 32
//									}

//									RosterIcon {
//										id: activityIcon
//										storage: "qrc:/activityicons"
//										iconData: activity
//										imageSize: 32
//									}
//								}
//							}

//							RowLayout {
//								Image {
//									id: statusIcon
//									source: "qrc:/statusicons/"+(statusiconset?statusiconset+"/":"")+show
//								}

//								SmallText {
//									text: status
//									wrapMode: Text.WordWrap
//									width: parent.width
//									font.italic: true
//								}
//							}

//                            Repeater {
//                                model: jids
//                                RowLayout {
//                                    id: jabberid

//                                    SmallText {
//                                        text: "Jabber ID:"
//                                        font.bold: true
//                                        opacity: rosterItem.detailsOpacity
//                                        visible: rosterItem.detailsOpacity
//                                    }

//                                    SmallText {
//                                        text: modelData
//                                        opacity: rosterItem.detailsOpacity
//                                        visible: rosterItem.detailsOpacity
//                                    }
//                                }
//                            }
//						}
            }

            Flickable {
                id: flick
                x: 10; width: parent.width - 20

                contentHeight: details.height
                clip: true

                anchors { top: topLayout.bottom; topMargin: 10; bottom: parent.bottom; bottomMargin: 10 }
                opacity: rosterItem.detailsOpacity

                ColumnLayout {
                    id: details
                    anchors.fill: parent

                    RosterText {
                        id: clientText
                        name: "Client"
                        iconData: client
                    }

                    RosterText {
                        id: moodText
                        iconData: mood
                        name: "Mood"
                    }

                    RosterText {
                        id: activityText
                        iconData: activity
                        name: "Activity"
                    }

                    RosterText {
                        id: annotationsText
                        iconData: annotations
                        storage: "qrc:/menuicons"
                        icon: "annotations"
                        multiline: true
                    }
                }
            }

            Image {
                anchors { right: flick.right; top: flick.top }
                source: "qrc:/menuicons/more-up"
                opacity: flick.atYBeginning ? 0 : rosterItem.detailsOpacity
            }

            Image {
                anchors { right: flick.right; bottom: flick.bottom }
                source: "qrc:/menuicons/more-down"
                opacity: flick.atYEnd ? 0 : rosterItem.detailsOpacity
            }

/*
            MouseArea {
                id: mouse
                hoverEnabled: true
                anchors.fill: parent
                onPressAndHold: rosterItem.state = 'Details'
                onEntered: rostersView.currentIndex = index
                enabled: rosterItem.state == "" // Disable mouse handling when
                                                // RosterItem is in Details mode
            }
*/
			Clickable {
				id: clickable
				anchors.fill: parent
				selectable: true
				enabled: rosterItem.state == "" // Disable mouse handling when
												// RosterItem is in Details mode
				onPressedAndHeld: rosterItem.state = 'Details'
				onPressed: rostersView.currentIndex = index
			}

            Drawer {
                id: rosterMenu
                position: Qt.RightEdge
				color: palette.button

				SimpleMenu {
					id: simpleMenu
					full: rosterItem.state=="Details"
					model: menu
					onClicked: {
						menu.itemSelected(id, rosterIndex)
						rosterMenu.hide()
					}
				}
			}

			// A button to close the detailed view, i.e. set the state back to default ('').
			TextButton {
				y: 10
				anchors { right: parent.right; rightMargin: 10 }
				opacity: rosterItem.detailsOpacity
				text: "Close"

				onClicked: rosterItem.state = '';
			}

			states: State {
				name: "Details"

				PropertyChanges { target: topLayout; height: avatarFullHeight + itemName.height} // Make picture bigger
				PropertyChanges { target: avatarFlick; width: topLayout.width; height: avatarFullHeight } // Make picture bigger
				PropertyChanges { target: rosterItem; detailsOpacity: 1; x: 0 } // Make details visible
				PropertyChanges { target: rosterItem; height: rostersView.height } // Fill the entire list area with the detailed view

				// Move the list so that this item is at the top.
				PropertyChanges { target: rosterItem.ListView.view; explicit: true; contentY: rosterItem.y }

				// Disallow flicking while we're in detailed view
				PropertyChanges { target: rosterItem.ListView.view; interactive: false }
			}

			transitions: Transition {
				// Make the state changes smooth
				ParallelAnimation {
					ColorAnimation { property: "color"; duration: 500 }
					NumberAnimation { duration: 300; properties: "detailsOpacity,x,contentY,height,width" }
				}
			}

			onStateChanged: if (state=="Details") clickable.state=""
        }
    }
/*
	Component {
		id: highlightBar
		Rectangle {
			width: rostersView.width; height: rostersView.currentItem.height

			gradient: Gradient {
				GradientStop {position: 0; color: palette.light}
				GradientStop {position: 1; color: palette.highlight}
			}
			opacity: 0.5;
			border.color: palette.highlight
			radius: 3
			y: rostersView.currentItem.y;
			Behavior on y { SpringAnimation { spring: 5; damping: 0.3 } }
		}
	}
*/
	// The actual list
	ListView {
		id: rostersView
		anchors.fill: parent
        model: rostersModel
		delegate: rosterItemDelegate
//		highlight: highlightBar
//		highlightFollowsCurrentItem: false
	}
}
