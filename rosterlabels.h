#ifndef ROSTERLABELS_H
#define ROSTERLABELS_H

#include <QObject>
#include <QtQml>
#include <QFont>
#include <QColor>

class RosterLabel : public QObject
{
	Q_OBJECT
	Q_PROPERTY(QString icon READ icon WRITE setIcon NOTIFY iconChanged)
	Q_PROPERTY(QString text READ text WRITE setText NOTIFY textChanged)
	Q_PROPERTY(QFont font READ font WRITE setFont NOTIFY fontChanged)
	Q_PROPERTY(QColor foreground READ foreground WRITE setForeground NOTIFY foregroundChanged)
	Q_PROPERTY(QColor background READ background WRITE setBackground NOTIFY backgroundChanged)
public:
	explicit RosterLabel(QObject *parent = NULL);
// icon property
	QString icon() const;
	void setIcon(const QString &icon);
// text property
	QString text() const;
	void setText(const QString &text);
// font property
	QFont font() const;
	void setFont(const QFont &font);
// foreground property
	QColor foreground() const;
	void setForeground(const QColor &color);
// background property
	QColor background() const;
	void setBackground(const QColor &color);

signals:
	void iconChanged();
	void textChanged();
	void fontChanged();
	void foregroundChanged();
	void backgroundChanged();
};
QML_DECLARE_TYPE(RosterLabel)

class RosterLabels : public QObject
{
	Q_OBJECT
	Q_PROPERTY(QList<RosterLabel *> topRight READ topRight NOTIFY topRightChanged)

public:
	explicit RosterLabels(QObject *parent = nullptr);
// topRight property
	QList<RosterLabel *> topRight();

signals:
	void topRightChanged();
};
QML_DECLARE_TYPE(RosterLabels)
#endif // ROSTERLABELS_H
