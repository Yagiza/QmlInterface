#ifndef SIMPLEMENUMODEL_H
#define SIMPLEMENUMODEL_H

#include <QtQml>

class SimpleMenuModel;

class SimpleMenuItem : public QObject {
	Q_OBJECT
	Q_PROPERTY(QString icon READ icon WRITE setIcon NOTIFY iconChanged)
	Q_PROPERTY(QString text READ text WRITE setText NOTIFY textChanged)
	Q_PROPERTY(int id READ id WRITE setId NOTIFY idChanged)

public:
	SimpleMenuItem(int id=-1, const QString &text=QString(), const QString &icon=QString(), QObject *parent=NULL);
	SimpleMenuItem(const SimpleMenuItem &other);

	int id() const;
	const QString &icon() const;
	const QString &text() const;

	void setId(int id);
	void setIcon(const QString &icon);
	void setText(const QString &text_);

signals:
	void idChanged(int);
	void textChanged(const QString &);
	void iconChanged(const QString &);

private:
	int _id;
	QString _text;
	QString _icon;
};
QML_DECLARE_TYPE(SimpleMenuItem)

class SimpleMenuGroup : public QObject {
	Q_OBJECT
	Q_PROPERTY(QList<QVariant> items READ items WRITE setItems NOTIFY itemsChanged)
public:
	SimpleMenuGroup(SimpleMenuModel *parent=NULL);
	const QList<QVariant> &items() const;
	void setItems(const QList<QVariant> &items);

	void addItem(SimpleMenuItem *item);
	SimpleMenuItem *addItem(int id, QString text, QString icon);
	bool removeItem(int index);
	SimpleMenuItem *item(int id);

signals:
	void itemsChanged();
private:
	QList<QVariant> _items;
};
QML_DECLARE_TYPE(SimpleMenuGroup)

class SimpleMenuModel : public QObject
{
	Q_OBJECT
	Q_PROPERTY(QList<QVariant> groups READ groups NOTIFY groupsChanged)

public:
	explicit SimpleMenuModel(QObject *parent = nullptr);
	QList<QVariant> groups() const;

	void addItem(int group, SimpleMenuItem *item);
	void addItem(int group, int id, QString text, QString icon=QString());

	SimpleMenuItem *item(int id);

signals:
	void groupsChanged();
	void itemSelected(int id, QObject *source);

public slots:

private:
	QMap<int, SimpleMenuGroup *> _groups;
};
QML_DECLARE_TYPE(SimpleMenuModel)

#endif // SIMPLEMENUMODEL_H
