#include "rostersmodel.h"

//
// IconData
//
RosterIconData::RosterIconData(QString icon, QString text, QObject *parent):
	QObject(parent), _icon(icon), _text(text)
{}

RosterIconData::RosterIconData(const RosterIconData &other):
	QObject(other.parent()), _icon(other._icon), _text(other._text)
{}

QString RosterIconData::icon() const
{
	return _icon;
}

QString RosterIconData::text() const
{
	return _text;
}

void RosterIconData::setIcon(const QString &icon_)
{
	if (_icon != icon_)
		emit iconChanged(_icon = icon_);
}

void RosterIconData::setText(const QString &text_)
{
	if (_text != text_)
		emit textChanged(_text = text_);
}

//
// RosterIndex
//
RosterIndex::RosterIndex()
{}

QVariant RosterIndex::data(int role)
{
	return role==IndexRole?QVariant::fromValue(this):itemData.value(role);
}

void RosterIndex::setData(int role, const QVariant &variant)
{
    itemData.insert(role, variant);
}

RostersModel::RostersModel(QObject *parent) : QAbstractListModel(parent)
{}

RosterIndex *RostersModel::findRecursively(const QModelIndex &parent) {
    if (parent.parent().isValid()) {
        return findRecursively(parent.parent())->children[parent.row()];
    } else {
        return rootIndexes[parent.row()];
    }
}

const RosterIndex *RostersModel::findRecursively(const QModelIndex &parent) const
{
    if (parent.parent().isValid()) {
        return findRecursively(parent.parent())->children[parent.row()];
    } else {
        return rootIndexes[parent.row()];
    }
}

QModelIndex RostersModel::findRecursively(RosterIndex *rosterIndex) const
{
    if (rosterIndex) {
        if (rosterIndex->parent) {
            QModelIndex mi = findRecursively(rosterIndex->parent);
            int row = rosterIndex->parent->children.indexOf(rosterIndex);
            if (row<-1)
                return index(row, 0, mi);
        } else {
            int row = rootIndexes.indexOf(rosterIndex);
            if (row>-1)
                return index(row);
        }
    }
    return QModelIndex();
}

QList<QByteArray> RostersModel::dataRoleNames(
    QList<QByteArray>()
		<< "rosterIndex"
		<< "name"
        << "avatar"
		<< "show"
		<< "statusiconset"
        << "status"
        << "annotations"
		<< "jids"
        << "resource"
		<< "mood"
		<< "activity"
		<< "tune"
		<< "location"
		<< "client"
        << "menu"
);

void RostersModel::addRosterItem(RosterIndex *rosterIndex, RosterIndex *parent)
{
    QModelIndex parentIndex = findRecursively(parent);
    beginInsertRows(parentIndex, rowCount(parentIndex), rowCount(parentIndex));
    if (parent) {
        parent->children << rosterIndex;
        rosterIndex->parent = parent;
    } else {
        rootIndexes << rosterIndex;
    }
    endInsertRows();
}

//
// === Implementation of QAbstractItemModel interface ===
//
int RostersModel::rowCount(const QModelIndex &parent) const
{
    return (parent.isValid()?findRecursively(parent)->children:rootIndexes).count();
}

QVariant RostersModel::data(const QModelIndex &index, int role) const
{
    if (index.isValid()) {
        if (index.parent().isValid())
            findRecursively(index.parent())->children[index.row()]->data(role);
        else
            return rootIndexes[index.row()]->data(role);
    }
    return QVariant();
}

QHash<int, QByteArray> RostersModel::roleNames() const
{
    QHash<int, QByteArray> roles;
	int i=RosterIndex::FirstRole;
    for (QList<QByteArray>::ConstIterator it=dataRoleNames.constBegin(); it!=dataRoleNames.constEnd(); ++it, ++i)
        roles.insert(i, *it);
    return roles;
}
