#ifndef ROSTERSMODEL_H
#define ROSTERSMODEL_H

#include <QAbstractListModel>
#include <QtQml>

//
// * RostersModel *
//
// Fields :
// nickname *
// show *
// status
// avatar *
// mood
// activity
// tune
// location
// client_icon
//
// full_jid
// annotations *
//

class RosterIconData : public QObject {
	Q_OBJECT
	Q_PROPERTY(QString icon READ icon WRITE setIcon NOTIFY iconChanged)
	Q_PROPERTY(QString text READ text WRITE setText NOTIFY textChanged)
public:
	RosterIconData(QString icon=QString(), QString text=QString(), QObject *parent = NULL);
	RosterIconData(const RosterIconData &other);

	QString icon() const;
	QString text() const;

	void setIcon(const QString &icon_);
	void setText(const QString &text_);

signals:
	void iconChanged(QString);
	void textChanged(QString);

private:
	QString _icon;
	QString _text;
};
QML_DECLARE_TYPE(RosterIconData)


class RosterIndex : public QObject
{
	Q_OBJECT
    friend class RostersModel;

public:
    enum Roles {		
		FirstRole = Qt::UserRole + 1,
		IndexRole = FirstRole,
		NameRole,
        AvatarRole,
		ShowRole,
		StatusiconsetRole,
		StatusRole,
        AnnotationsRole,
		JidsRole,
		ResourceRole,
		MoodRole,
		ActivityRole,
		TuneRole,
		LocationRole,
		ClientRole,
		MenuRole,
    };

    RosterIndex();

	QVariant data(int role);
    void setData(int role, const QVariant &variant);

private:
    RosterIndex *parent;
    QList<RosterIndex *> children;
    QHash<int, QVariant> itemData;
};
QML_DECLARE_TYPE(RosterIndex)

class RostersModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit RostersModel(QObject *parent = nullptr);

    void addRosterItem(RosterIndex *rosterIndex, RosterIndex *parent = NULL);

    // QAbstractItemModel interface
    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const;

protected:
    RosterIndex *findRecursively(const QModelIndex &parent);
    const RosterIndex *findRecursively(const QModelIndex &parent) const;
    QModelIndex findRecursively(RosterIndex *rosterIndex) const;

signals:

private:
    QList<RosterIndex *> rootIndexes;
    static QList<QByteArray> dataRoleNames;
};

#endif // ROSTERSMODEL_H
